﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phonebook
{
    class Generator : Contact
    {
        private int[] cods = { 43, 91, 96, 99, 93, 77, 94, 98, 55, 95, 41 };
        private string[] gender = { "Female", "Male" };                  
        private string[] proff = { "Doctor","Policeman","Musician","Lawyer","Programmer","Batman"};
        Random rand = new Random();
        public List<Contact> PhoneB(int count)
        {                                                                              
            List<Contact> con = new List<Contact>(count);                              
            for (int i = 0; i < count; i++)                                            //orange - 55 95 41    
            {
                Contact c = new Contact();
                c.FullName = Fullname();
                c.Gender = Gender();
                c.Code = GetCode();
                c.Number = $"(+374) (0){c.Code}-{Number()}";
                
                c.Profession = Proff();
                con.Add(c);
            }
            return con;
        }
        private string Fullname()
        {

            string fullname = $"A-{rand.Next(1, 100)}yan";
            return fullname;
        }
        private string Gender()
        {
            return gender[rand.Next(0, gender.Length)];
        }
        public int GetCode()
        {
            return cods[rand.Next(0, cods.Length)];
        }
        private string Number()
        {
            string nu = string.Format("{0:##-##-##}", rand.Next(100000,1000000));
            return nu;
        }
        private string Proff()
        {
            return proff[rand.Next(0, proff.Length)];
        }
    }
}
