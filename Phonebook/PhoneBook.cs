﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;                                                    
using System.Threading.Tasks;                                         
                                                                      
namespace Phonebook
{
    static public class PhoneBook
    {
        static public void GenderSearch(List<Contact> con, string gen)
        {
            for (int i = 0; i < con.Count; i++)
            {
                if (con[i].Gender == gen)
                    Print(con[i]);
            }
        }
        static public void OperatorSearch(List<Contact> con, string op)          //Beeline - 43 91 96 99
        {                                                                        // Viva - 93 77 94 98  
            for (int i = 0; i < con.Count; i++)                                  //orange - 55 95 41    
            {
                switch (op)
                {
                    case "Vivacell":
                        if (con[i].Code == 93 || con[i].Code == 77 || con[i].Code == 94 || con[i].Code == 98)
                        Print(con[i]);
                        break;
                    case "Beeline":
                        if (con[i].Code == 43 || con[i].Code == 91 || con[i].Code == 96 || con[i].Code == 99)
                        Print(con[i]);
                        break;
                    case "Ornage":
                        if (con[i].Code == 55 || con[i].Code == 95 || con[i].Code == 41)
                            Print(con[i]);
                                break;
                }
            }
        }
        static public void ProfSearch(List<Contact> con, string prof)
        {
            for (int i = 0; i < con.Count; i++)
            {
                if (con[i].Profession == prof)
                    Print(con[i]);
            }
        }
        static public void Print(Contact cont)
        {
            Console.WriteLine(cont.FullName + "\t\t" + cont.Gender + "\t\t" + cont.Number + "\t" + cont.Profession);
        }
    }
}
