﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phonebook
{
    
    public class Program
    {
        static void Main(string[] args)
        {
            Generator f = new Generator();
            List<Contact> book = f.PhoneB(20);
            Print(book);

            Search("Gender Search");
            PhoneBook.GenderSearch(book,"Male");
            Search("Operator Search");
            PhoneBook.OperatorSearch(book, "Beeline");
            Search("Profession Search");
            PhoneBook.ProfSearch(book, "Musician");
            Console.Read();
        }
        static public void Print(List<Contact> cont)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Fullname" + "\t" + "Gender" + "\t\t" + "Phone number" + "\t\t" + "Profesion" + "\n");
            Console.ResetColor();
            foreach (Contact item in cont)
            {
                Console.WriteLine(item.FullName + "\t\t" + item.Gender +"\t\t" + item.Number + "\t" + item.Profession);
            }
        }
        static public void Search(string s)
        {
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"{s} \n");
            Console.ResetColor();
        }
    }
}
