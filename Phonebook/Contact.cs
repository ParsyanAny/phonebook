﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phonebook
{
    public class Contact
     {
        public string FullName { get; set; } 
        public string Gender { get; set; }
        public string Number { get; set; }
        public string Profession { get; set; }
        public int Code { get; set; }
    }
}